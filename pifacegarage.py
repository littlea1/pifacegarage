from __future__ import division
from time import sleep
import pifacedigitalio as pfio
from flask import Flask, render_template

pfio.init()


app = Flask(__name__)


@app.route('/')
def help():
    return render_template('buttons.html')


@app.route('/doorleft', methods=['GET', 'POST'])
def click_door_left():
    '''Close relay 0 for 1 second '''
    pfio.digital_write(0, 1)
    sleep(1)
    pfio.digital_write(0, 0)
    return render_template('buttons.html')


@app.route('/doorright', methods=['GET', 'POST'])
def click_door_right():
    '''Close relay 1 for 1 second '''
    pfio.digital_write(1, 1)
    sleep(1)
    pfio.digital_write(1, 0)
    return render_template('buttons.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
